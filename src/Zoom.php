<?php

namespace SchoolTracs\Zoom;

use Exception;

class Zoom {

  /**
   * @var null
   */
  private $accessToken = null;

  /**
   * Zoom constructor.
   * @param $accessToken
   */
  public function __construct($accessToken) {
    $this->accessToken = $accessToken;
  }

  /**
   * __call
   *
   * @param $method
   * @param $args
   * @return mixed
   */
  public function __call($method, $args) {
    return $this->make($method);
  }

  /**
   * __get
   *
   * @param $name
   * @return mixed
   */
  public function __get($name) {
    return $this->make($name);
  }
  /**
   * Make
   *
   * @param $resource
   * @return mixed
   * @throws Exception
   */
  public function make($resource) {

    $class = 'SchoolTracs\\Zoom\\Endpoint\\' . ucfirst(strtolower($resource));
    if (class_exists($class)) {
      return new $class($this->apiKey, $this->apiSecret);
    }
    throw new Exception('Wrong method');
  }
}