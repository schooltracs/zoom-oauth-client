<?php

namespace SchoolTracs\Zoom\Endpoint;

use SchoolTracs\Zoom\Http\Request;

/**
 * Class Meetings
 * @package SchoolTracs\Zoom\Endpoint
 */
class Meetings extends Request {

  const Type_Instant_Meeting = 1;
  const Type_Scheduled_Meeting = 2;
  const Type_Recurring_Meeting_Without_Fixed_Time = 3;
  const Type_Recurring_Meeting_With_Fixed_Time = 8;

  const Recurrence_Daily = 1;
  const Recurrence_Weekly = 2;
  const Recurrence_Monthly = 3;

  const Week_Day_Sunday = 1;
  const Week_Day_Monday = 2;
  const Week_Day_Tuesday = 3;
  const Week_Day_Wednesday = 4;
  const Week_Day_Thursday = 5;
  const Week_Day_Friday = 6;
  const Week_Day_Saturday = 7;

  const Monthly_Last_Week = -1;
  const Monthly_First_Week = 1;
  const Monthly_Second_Week = 2;
  const Monthly_Third_Week = 3;
  const Monthly_Fourth_Week = 4;

  const Approval_Automatic = 0;
  const Approval_Manual = 1;
  const Approval_None = 2;

  const Registration_Once_For_All = 1;
  const Registration_Everytime = 2;
  const Registration_Once_For_Some = 3;

  const Audio_Both = 'both';
  const Audio_Telephony = 'telephone';
  const Audio_VoIP = 'voip';

  const Auto_Record_Local = 'local';
  const Auto_Record_Cloud = 'cloud';
  const Auto_Record_None = 'none';

  /**
   * Meetings constructor.
   * @param $accessToken
   */
  public function __construct($accessToken) {
    parent::__construct($accessToken);
  }

  /**
   * List
   *
   * @param $userId
   * @param array $query
   * @return array|mixed
   */
  public function list(string $userId, array $query = []) {
    return $this->_get("users/{$userId}/meetings", $query);
  }

  /**
   * Create
   *
   * @param $userId
   * @param array $data
   * @return array|mixed
   */
  public function create(string $userId, array $data = null) {
    return $this->_post("users/{$userId}/meetings", $data);
  }

  /**
   * Get
   *
   * @param $meetingId
   * @return array|mixed
   */
  public function get(string $meetingId) {
    return $this->_get("meetings/{$meetingId}");
  }

  /**
   * Delete
   *
   * @param $meetingId
   * @return array|mixed
   */
  public function delete(string $meetingId) {
    return $this->_delete("meetings/{$meetingId}");
  }

  /**
   * Update
   *
   * @param $meetingId
   * @param array $data
   * @return array|mixed
   */
  public function update(string $meetingId, array $data = []) {
    return $this->_patch("meetings/{$meetingId}", $data);
  }

  /**
   * Update Status
   *
   * @param $meetingId
   * @param array $data
   * @return mixed
   */
  public function status(string $meetingId, array $data = []) {
    return $this->_put("meetings/{$meetingId}/status", $data);
  }

  /**
   * List Registrants
   *
   * @param $meetingId
   * @param array $query
   * @return array|mixed
   */
  public function listRegistrants(string $meetingId, array $query = []) {
    return $this->_get("meetings/{$meetingId}/registrants", $query);
  }

  /**
   * Add Registrant
   *
   * @param $meetingId
   * @param array $data
   * @return array|mixed
   */
  public function addRegistrant(string $meetingId, $data = []) {
    return $this->_post("meetings/{$meetingId}/registrants", $data);
  }

  /**
   * Update Registrant Status
   *
   * @param $meetingId
   * @param array $data
   * @return array|mixed
   */
  public function updateRegistrantStatus(string $meetingId, array $data = []) {
    return $this->_put("meetings/{$meetingId}/registrants/status", $data);
  }

  /**
   * Past Meeting
   *
   * @param $meetingUUID
   * @return array|mixed
   */
  public function pastMeeting(string $meetingUUID) {
    return $this->_get("past_meetings/{$meetingUUID}");
  }

  /**
   * Past Meeting Participants
   *
   * @param $meetingUUID
   * @param array $query
   * @return array|mixed
   */
  public function pastMeetingParticipants(string $meetingUUID, array $query = []) {
    return $this->_get("past_meetings/{$meetingUUID}/participants", $query);
  }

}