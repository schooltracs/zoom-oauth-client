<?php

namespace SchoolTracs\Zoom\Endpoint;

use SchoolTracs\Zoom\Http\Request;

/**
 * Class Recordings
 * @package SchoolTracs\Zoom\Endpoint
 */
class Recordings extends Request {

  /**
   * Recordings constructor.
   * @param $accessToken
   */
  public function __construct($accessToken) {
    parent::__construct($accessToken);
  }

  /**
   * List All
   *
   * @param $userId
   * @param array $query
   * @return array|mixed
   */
  public function listAll(string $userId, array $query = []) {
    return $this->_get("users/{$userId}/recordings", $query);
  }

  /**
   * List Meeting Recordings
   *
   * @param $meetingId
   * @return array|mixed
   */
  public function meeting(string $meetingId) {
    return $this->_get("meetings/{$meetingId}/recordings");
  }

  /**
   * Delete All
   *
   * @param $meetingId
   * @param array $query
   * @return array|mixed
   */
  public function deleteAll(string $meetingId, array $query = ['action' => 'trash']) {
    return $this->_delete("meetings/{$meetingId}/recordings", $query);
  }

  /**
   * Delete
   *
   * @param $meetingId
   * @param $recordingId
   * @param array $query
   * @return array|mixed
   */
  public function delete(string $meetingId, string $recordingId, array $query = ['action' => 'trash']) {
    return $this->_delete("meetings/{$meetingId}/recordings/{$recordingId}", $query);
  }

  /**
   * Recover All
   *
   * @param $meetingId
   * @param array $data
   * @return array|mixed
   */
  public function recoverAll(string $meetingId, array $data = ['action' => 'recover']) {
    return $this->_put("meetings/{$meetingId}/recordings/status", $data);
  }

  /**
   * Recover
   *
   * @param $meetingId
   * @param $recordingId
   * @param array $data
   * @return array|mixed
   */
  public function recover(string $meetingId, string $recordingId, array $data = ['action' => 'recover']) {
    return $this->_put("meetings/{$meetingId}/recordings/{$recordingId}/status", $data);
  }

}