<?php

namespace SchoolTracs\Zoom\Endpoint;

use SchoolTracs\Zoom\Http\Request;

/**
 * Class Users
 * @package SchoolTracs\Zoom\Endpoint
 */
class Users extends Request {

  /**
   * Users constructor.
   * @param $accessToken
   */
  public function __construct($accessToken) {
    parent::__construct($accessToken);
  }

  /**
   * List
   *
   * @param array $query
   * @return array|mixed
   */
  public function list(array $query = []) {
    return $this->_get("users", $query);
  }

  /**
   * Create
   *
   * @param array|null $data
   * @return array|mixed
   */
  public function create(array $data = null) {
    return $this->_post("users", $data);
  }

  /**
   * Get
   *
   * @param $userId
   * @param array $query
   * @return array|mixed
   */
  public function get(string $userId, array $query = []) {
    return $this->_get("users/{$userId}", $query);
  }

  /**
   * Delete
   *
   * @param $userId
   * @return array|mixed
   */
  public function delete(string $userId) {
    return $this->_delete("users/{$userId}");
  }

  /**
   * Update
   *
   * @param $userId
   * @param array $data
   * @return array|mixed
   */
  public function update(string $userId, array $data = []) {
    return $this->_patch("users/{$userId}", $data);
  }
}